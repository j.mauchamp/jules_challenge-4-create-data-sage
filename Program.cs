﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Objets100cLib;

namespace Challenge_4_create_sage
{
    class Program
    {
        static void Main(string[] args)
        {

            //on se connecte a la base
            var gescomDb = OpenDB(@"C:\Users\Public\Documents\Sage\Entreprise 100c\Bijou.gcm", "<Administrateur>", "");
            Console.WriteLine("Ouverture de la base");

            //on creer l'article
            var newProd = (IBOArticle3)gescomDb.FactoryArticle.Create();
            var famillenewProd = gescomDb.FactoryFamille.ReadCode(FamilleType.FamilleTypeDetail, "BIJOUXARG");
            //ajout catalogue bague
            var cataloguenewProd = gescomDb.FactoryProduit.ReadIntitule("Bijoux").FactorySousCatalogue.ReadIntitule("Argent").FactorySousCatalogue.ReadIntitule("Bagues");

            //info de l'article
            newProd.AR_Type = ArticleType.ArticleTypeStandard;
            newProd.AR_Ref = "TESTART1";
            newProd.AR_Design = "Bague en argent";
            newProd.Famille = famillenewProd ;
            newProd.AR_CodeBarre = "JOHNDOE3456";
            newProd.AR_PrixAchat = 500;
            newProd.AR_PrixVen = 1000;
            newProd.Catalogue = cataloguenewProd;

            newProd.AR_SuiviStock = SuiviStockType.SuiviStockTypeCmup;
            newProd.AR_Langue1 = "silver ring";

            //on créer un test pour voir si l'article existe deja
            if (gescomDb.FactoryArticle.ExistReference("TESTART1") == false)
            {
                newProd.SetDefault();
                newProd.Write();

                newProd.InfoLibre["Pourcentage teneur en or"] = 15;
                newProd.Write();
                Console.WriteLine("l'article a été créé dans la base");
            }
            else
            {
                Console.WriteLine("le produit existe deja");
            }

            //Creation nouveau client
            var newCustomer = (IBOClient3)gescomDb.CptaApplication.FactoryClient.Create();

            newCustomer.CT_Num = "CTCHALL";
            newCustomer.CT_Intitule = "Client Challenge";
            newCustomer.Adresse.Adresse = "Forum des halles";
            newCustomer.Adresse.Complement = "82 rue des lions";
            newCustomer.Adresse.Ville = "Lyon";
            newCustomer.Adresse.CodePostal = "69008";
            newCustomer.CT_EdiCode = "EDI123456";

            if (gescomDb.CptaApplication.FactoryClient.ExistNumero("CTCHALL") == false)
            {
                newCustomer.SetDefault();
                newCustomer.Write();

                Console.WriteLine("le client a été créé dans la base");
            }
            else
            {
                Console.WriteLine("le client existe deja");
            }

            //ajout d'un document dans la base
            var customer = gescomDb.CptaApplication.FactoryClient.ReadNumero("CTCHALL");
            var newFile = (IBODocumentVente3)gescomDb.FactoryDocumentVente.CreateType(DocumentType.DocumentTypeVenteDevis);

            newFile.SetDefaultClient(customer);
            newFile.Write();

            //création lignes
            var newFileline1 = (IBODocumentVenteLigne3)newFile.FactoryDocumentLigne.Create();
            //on va chercher la ref de l'article que l'on veut cibler et on la stock dans une variable
            var article1 = gescomDb.FactoryArticle.ReadReference("GRAVURE");
            newFileline1.SetDefaultArticle(article1, 2);
            newFileline1.SetDefault();
            try
            {
                newFileline1.Write();
                Console.WriteLine("ligne créée avec succès.");
            }
            catch
            {
                Console.WriteLine("erreur dans la création de la ligne");
            }

            var newFileline2 = (IBODocumentVenteLigne3)newFile.FactoryDocumentLigne.Create();
            //on va chercher la ref de l'article que l'on veut cibler et on la stock dans une variable
            var article2 = gescomDb.FactoryArticle.ReadReference(newProd.AR_Ref);
            newFileline2.SetDefaultArticle(article2, 12);
            //on donne la remise sous forme de string
            newFileline2.Remise.FromString("5%");
            newFileline2.SetDefault();
            try
            {
                newFileline2.Write();
                Console.WriteLine("ligne créée avec succès.");
            }
            catch
            {
                Console.WriteLine("erreur dans la création de la ligne");
            }

            //transformer un bon de commande en bon de livraison 
            if(gescomDb.FactoryDocumentVente.ExistPiece(DocumentType.DocumentTypeVenteCommande, "BC00033"))
            {
                //creer le processus
                var tranformer = (IPMDocTransformer)gescomDb.Transformation.Vente.CreateProcess_Livrer();
                //ajout doc au processus
                var addDoc = (IBODocument3)gescomDb.FactoryDocumentVente.ReadPiece(DocumentType.DocumentTypeVenteCommande, "BC00033");
                tranformer.AddDocument(addDoc);
                //exectue le processe
                if (tranformer.CanProcess)
                {
                    tranformer.Process();
                    Console.WriteLine("t'as réussis a transformer");
                }
                else
                {
                    Console.WriteLine("pas réussis a process");
                }
            }
            else
            {
                Console.WriteLine("document inexistant");
            }

            //transformer un bon de livraison en facture 
            if(gescomDb.FactoryDocumentVente.ExistPiece(DocumentType.DocumentTypeVenteLivraison, "BL00005"))
            {
                //creer le processus
                var tranformer2 = (IPMDocTransformer)gescomDb.Transformation.Vente.CreateProcess_Facturer();
                //ajout doc au processus
                var addDoc2 = (IBODocument3)gescomDb.FactoryDocumentVente.ReadPiece(DocumentType.DocumentTypeVenteLivraison, "BL00005");
                tranformer2.AddDocument(addDoc2);
                //exectue le processe
                if (tranformer2.CanProcess)
                {
                    tranformer2.Process();
                    Console.WriteLine("t'as réussis a transformer");
                }
                else
                {
                    Console.WriteLine("pas réussis a process");
                }
            }
            else
            {
                Console.WriteLine("document inexistant");
            }
            Console.Read();
        }

        //création d'une fonction pour ouvrir la base.
        public static BSCIALApplication100c OpenDB(string filepath, string user, string password)
        {
            var BaseCom = new BSCIALApplication100c();

            BaseCom.Name = filepath;
            BaseCom.Loggable.UserName = user;
            BaseCom.Loggable.UserPwd = password;
            BaseCom.Open();

            return BaseCom;
        }

    }
}
